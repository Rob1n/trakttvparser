function onError(error) {
	console.log(`Error: ${error}`);
}

function onGotBaseUrl(item) {
    var quickIcons = document.querySelectorAll(".quick-icons");
	for (var i = 0; i < quickIcons.length; i++) {
        // skipping smaller thumbs on bottom that have other classes 
        if (quickIcons[i].classList != "quick-icons") {
            continue;
        }
        
        var showRow = quickIcons[i].parentElement.parentElement;
        //console.log("showRow: " + showRow);
        
        // show name
        var showName = showRow.querySelector(".show-title").firstChild.innerHTML;
        showName = showName.replace("'", "").replace(":", "");
        //console.log("showName: " + showName);
        
        // episode number
        var sxe = quickIcons[i].parentElement.querySelector(".main-title-sxe").innerHTML;
        var season = sxe.split("x")[0];
		season = ("0" + season).slice(-2);
		var episode = sxe.split("x")[1];
        
        // query
		var searchQuery = showName + " S" + season + "E" + episode;
        //console.log("query: " + searchQuery);
		
        // copy to cliboard
        var btnTag = document.createElement('a');
        btnTag.innerHTML = "c";
        //btnTag.setAttribute("id", "btn"+i);
        btnTag.setAttribute("href", "javascript:void(0);");
        btnTag.setAttribute("data-content", searchQuery);
        btnTag.onclick = function() {
            //console.log(this.dataset.content);
            //window.prompt("Copy to clipboard: Ctrl+C, Enter", this.dataset.content);
            var $temp = document.createElement('input'); //var temp = $("<input>").val(this.dataset.content);
            this.appendChild($temp);
            $temp.value = this.dataset.content;
            $temp.select();
            document.execCommand("copy");
            $temp.remove();
        }
        quickIcons[i].querySelector(".actions").appendChild(btnTag);
        
        // search for name in new tab
        var aTag = document.createElement('a');
		aTag.setAttribute('href', item.traktTvUrl + searchQuery);
		aTag.setAttribute('target', "_blank");
		aTag.innerHTML = "t";
		quickIcons[i].querySelector(".actions").appendChild(aTag);        
	}
}

console.log("Started watched script");
var getBaseUrl = browser.storage.local.get("traktTvUrl");
getBaseUrl.then(onGotBaseUrl, onError);



// popup
/*
function getActiveTab() {
    return browser.tabs.query({active: true, currentWindow: true});
}

getActiveTab().then((tabs) => {
    var list = document.getElementById('history');
    var hostname = get_hostname(tabs[0].url);
    
    console.log(list);
    console.log(hostname);
  
});

console.log(browser);
console.log(browser.pageAction);
browser.pageAction.show();
pageAction.show();
*/
