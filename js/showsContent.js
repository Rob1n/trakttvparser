function onError(error) {
	console.log(`Error: ${error}`);
}

function onGot(item) {
    var quickIcons = document.querySelectorAll(".quick-icons");
    var showName = document.querySelector("[property='og:title']").getAttribute("content");
    
	for (var i = 0; i < quickIcons.length; i++) {
        // skipping smaller thumbs on bottom that have other classes 
        if (quickIcons[i].classList != "quick-icons") {
            continue;
        }
        
        // sxe
        var sxe = quickIcons[i].parentElement.querySelector(".main-title-sxe").innerHTML;
        var season = sxe.split("x")[0];
		season = ("0" + season).slice(-2);
		var episode = sxe.split("x")[1];
        
        // query
		var searchQuery = showName + " S" + season + "E" + episode;
		
        // element
		var aTag = document.createElement('a');
		aTag.setAttribute('href', item.traktTvUrl + searchQuery);
		aTag.setAttribute('target', "_blank");
		aTag.innerHTML = "t";
		quickIcons[i].querySelector(".actions").appendChild(aTag);
        
	}
}

console.log("Started shows script");
var getting = browser.storage.local.get("traktTvUrl");
getting.then(onGot, onError);

//browser.pageAction.show(browser.tabs.getCurrent());
