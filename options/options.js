function saveOptions(e) {
	e.preventDefault();
	browser.storage.local.set({
		traktTvUrl: document.querySelector("#traktTvUrl").value
	});
}

function restoreOptions() {

	function setCurrentChoice(result) {
		document.querySelector("#traktTvUrl").value = result.traktTvUrl || "https://---/search/";
	}

	function onError(error) {
		console.log(`Error: ${error}`);
	}

	var getting = browser.storage.local.get("traktTvUrl");
	getting.then(setCurrentChoice, onError);
}

document.addEventListener("DOMContentLoaded", restoreOptions);
document.querySelector("form").addEventListener("submit", saveOptions);